#ifndef Imb_h
#define Imb_h

#include <Arduino.h>
#include <Stream.h>

// imb protocol definitions
const byte imbMagic = 0xFE;
const int imbMinimumPacketSize = 16;
const int imbMaximumPayloadSize = 10*1024*1024;
const unsigned int INVALID_EVENT_ID = (unsigned int)-1;

// google protocol wire types
const int wtVarInt=0;            // int32, int64, uint32, uint64, sint32, sint64, bool, enum
const int wt64Bit=1;             // double or fixed int64/uint64
const int wtLengthDelimited=2;   // string, bytes, embedded messages, packed repeated fields
const int wtStartGroup=3;        // deprecated
const int wtEndGroup=4;          // deprecated
const int wt32Bit=5;             // float (single) or fixed int32/uint32

// imb states
const unsigned int icsUninitialized=0;
const unsigned int icsInitialized=1;
const unsigned int icsClient=2;
const unsigned int icsHub=3;
const unsigned int icsEnded=4;
const unsigned int icsTimer=10;
// room for extensions ..
const unsigned int icsGateway=100;

// tags
const unsigned int icehRemark = 1;                      // <string>

// subscribe/publish
const unsigned int icehSubscribe = 2;                   // <uint32: varint>
const unsigned int icehPublish = 3;                     // <uint32: varint>
const unsigned int icehUnsubscribe = 4;                 // <uint32: varint>
const unsigned int icehUnpublish = 5;                   // <uint32: varint>
const unsigned int icehSetEventIDTranslation = 6;       // <uint32: varint>
const unsigned int icehEventName = 7;                   // <string>
const unsigned int icehEventID = 8;                     // <uint32: varint>

// connection
const unsigned int icehUniqueClientID = 11;             // <guid>
const unsigned int icehHubID = 12;                      // <guid>
const unsigned int icehModelName = 13;                  // <string>
const unsigned int icehModelID = 14;                    // <int32: varint> ?
const unsigned int icehReconnectable = 15;              // <bool: varint>
const unsigned int icehState = 16;                      // <uint32: varint>
const unsigned int icehEventNameFilter = 17;            // <string>
const unsigned int icehNoDelay = 18;                    // <bool: varint>
const unsigned int icehClose = 21;                      // <bool: varint>
const unsigned int icehReconnect = 22;                  // <guid>


// standard event tags

// basic event tags
const unsigned int icehIntString = 1;                   // <varint>
  const unsigned int icehIntStringPayload = 2;          // <string>
const unsigned int icehString = 3;                      // <string>
const unsigned int icehChangeObject = 4;                // <int32: varint>
  const unsigned int icehChangeObjectAction = 5;        // <int32: varint>
  const unsigned int icehChangeObjectAttribute = 6;     // <string>

// stream
const unsigned int icehStreamHeader = 7;                // <string> filename
const unsigned int icehStreamBody = 8;                  // <bytes>
const unsigned int icehStreamEnd = 9;                   // <bool> true: ok, false: cancel
  const unsigned int icehStreamID = 10;                 // <id: bytes/string>


void printGUIDCompact(Stream &aStream, byte* aGUID);

class IMB
{
public:
  IMB(const char* aModelName, int aModelID, bool aReconectable, Stream* aStream, bool aUseCheckSum, Stream* aDebugStream=0);
protected:
  const char* fModelName;
  int fModelID;
  bool fReconectable;
  Stream* fStream;
  Stream* fDebugStream;
  bool fConnected;
  bool fWritingEnabled;
  // event translation
  unsigned int* fRemoteToLocalEventID;
  unsigned int fRemoteToLocalEventIDLength;
  void AddOrSetRemoteToLocalEventID(unsigned int aRemoteEventID, unsigned int aLocalEventID);
  unsigned int TranslateEventID(unsigned int aRemoteEventID);
  // connect info retrieved 
  byte uniqueClientID[16];
  byte hubID[16];
  // stats
  int fCommandsReceived;
  int fCommandsSend;
  int fEventsReceived;
  int fEventsSend;
  int fErr;
  int fMissedMagic;
  int fCheckSum;
  bool fUseCheckSum;
  void FlushCommand(byte* aBuffer, int aSize);
  bool testCheckSum(int aFillerBytes);
public:
  int getCommandsReceived() { return fCommandsReceived; };
  int getCommandsSend() { return fCommandsSend; };
  int getEventsReceived() { return fEventsReceived; };
  int getEventsSend() { return fEventsSend; };
  int getErr() { return fErr; };
  int getMissedMagic() { return fMissedMagic; };
  bool getWritingEnabled() { return fWritingEnabled; };
  virtual bool getConnected() { return fConnected; };
  void setConnected(bool aValue) { fConnected = aValue; fWritingEnabled = false; };
public:
  int Read(unsigned int &aValue);
  int Read(int &aValue);
  int Read(float &aValue);
  int Read(byte* aValue, int aValueSize);
  int Read(String &aValue);

  int SkipBytes(int aByteCount);
  
  int BufferRead(byte* &aBuffer, unsigned int &aValue);
  int BufferRead(byte* &aBuffer, int &aValue);
  int BufferRead(byte* &aBuffer, float &aValue);
  int BufferRead(byte* &aBuffer, byte* aValue, int aValueSize);
  int BufferRead(byte* &aBuffer, String& aValue);

  int BufferReadGUID(byte* &aBuffer, byte* aGUID)
  
  int Prepare(unsigned int aValue);
  int Prepare(int aValue);
  int Prepare(float aValue);
  int Prepare(char* aValue);
  
  int BufferWrite(byte* &aBuffer, unsigned int aValue);
  int BufferWrite(byte* &aBuffer, int aValue);
  int BufferWrite(byte* &aBuffer, float aValue);
  int BufferWrite(byte* &aBuffer, byte* aValue, int aValueSize);
  int BufferWrite(byte* &aBuffer, char* aValue);

  int ReadGUID(byte* aValue);
  int ReadEventIDFixed(unsigned int &aEventIDFixed);
  
  int BufferWriteGUIDEmpty(byte* &aBuffer); // empty GUID all zeros 
  void BufferWriteFillingBytes(byte* &aBuffer, int aSizeCommand);
public:
  // signal event/command
  void SignalConnect(const char* aModelName, int aModelID, bool aReconnectable);
  void SignalSubscribe(unsigned int aEventID, const char* aEventName);
  void SignalPublish(unsigned int aEventID, const char* aEventName);
  void SignalUnSubscribe(unsigned int aEventID);
  void SignalUnPublish(unsigned int aEventID);
  void SignalNoDelay(bool aNoDelay);
  void SignalClose();
  void SignalHeartBeat();
  void SignalEvent(unsigned int aEventID, byte* aData, int aSizeData);
protected:
  void HandleEvent(unsigned int aEventID, byte* aData, int aDataSize);
  void HandleCommand(byte* aData, int aDataSize);
public:
  // higher level imb command/event handling
  void Close() { HandleCommandClose(); };
  void HandlePacket();
  // empty handlers to override when needed
  //virtual void HandleEvent(unsigned int aEventID, byte* aEventData, int aEventDataSize) {};
  virtual void HandleEventTag(unsigned int aEventID, unsigned int tag_and_wiretype, byte* aData) {};
  virtual void HandleCommandTag(unsigned int tag_and_wiretype, byte* aData) {};
  virtual void HandleCommandSetEventIDTranslation(unsigned int remoteEventID, unsigned int localEventID) {};
  virtual void HandleCommandClose() {};
  virtual void HandleCommandConnect(byte* uniqueClientID, byte* hubID) {};
  virtual void HandleDidNotFindMagic(byte aReceivedByte) {};
  virtual int HandleOtherCommand(int aCommand, int aCommandPayloadSize) { return 0; };
  virtual void HandleInit(String aHeader) {};
  virtual void HandleCheckSumError() {};
};

#endif

