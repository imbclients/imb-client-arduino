#include <SPI.h>
#include <Ethernet.h>
#include "Imb.h"

// configurable settings

// connection to hub (TCP)
IPAddress imbHub(192,168,1,100);
int imbPort = 4000;

// model name and id
const int modelID = 4;
const char modelName[] = "Arduino";

// ethernet MAC address
byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };

// event id's
const int eiTestArduino1 = 0;
const int eiTestArduino2 = 1;


// TCP socket
EthernetClient socket;

// derive class to override HandleEvent
class IMBsocket:public IMB
{
public:
  IMBsocket(const char* aModelName, int aModelID, Stream* aStream, Stream* aDebugStream=0) : IMB(aModelName, aModelID, true, aStream, true, aDebugStream) 
  {
    lastSignalMillis = 0;
    lastHeartBeatMillis = 0;
  };
  // override handler functions
  virtual void HandleEvent(unsigned int aEventID, byte* aEventData, int aEventDataSize)
  {
    // handle event
    String s;
    int i;
    float f;
    BufferRead(aEventData, s);
    BufferRead(aEventData, i);
    BufferRead(aEventData, f);
  };
  virtual void HandleCommandSetEventIDTranslation(unsigned int remoteEventID, unsigned int localEventID) 
  {
    Serial.print("icSetEventIDTranslation: ");
    Serial.print(localEventID);
    Serial.print(" -> ");
    Serial.println(remoteEventID);
  };
  virtual bool getConnected() { return fConnected && socket.connected(); };
  virtual void HandleCommandClose() 
  {
    if (socket.connected())
      socket.stop();
  };
  virtual void HandleCommandConnect(byte* uniqueClientID, byte* hubID) 
  {
    if (fDebugStream)
    {
      fDebugStream->print(" icConnect(");
      printGUIDCompact(*fDebugStream, uniqueClientID);
      fDebugStream->print(",");
      printGUIDCompact(*fDebugStream, hubID);
      fDebugStream->print(") ");
      fDebugStream->println();
    }
  };
  virtual void HandleInit(String aHeader)
  {
    SignalSubscribe(eiTestArduino1, "UST.TestArduino1");
    SignalPublish(eiTestArduino2, "UST.TestArduino2");
  };
  virtual void HandleDidNotFindMagic(byte aReceivedByte) 
  {
    if (fDebugStream)
    {
      fDebugStream->print("##M(");
      fDebugStream->print(aReceivedByte);
      if (aReceivedByte>=32 && aReceivedByte<128)
      {
        fDebugStream->print(":");
        fDebugStream->print(char(aReceivedByte));
      }
      fDebugStream->println(")");
    }
  };
  virtual int HandleOtherCommand(int aCommand, Stream &aStream, int aCommandPayloadSize) 
  {
    int res=0;
    if (fDebugStream)
    {
      switch (aCommand)
      {
        //case icConnect = -1;
        //case icClose = -2;
        case icSubscribe:
          //fDebugStream->print(" icSubscribe ");
          break;
        case icPublish:
          //fDebugStream->print(" icPublish ");
          break;
        case icUnsubscribe:
          //fDebugStream->print(" icUnsubscribe ");
          break;
        case icUnpublish:
          //fDebugStream->print(" icUnpublish ");
          break;
        //case icSetEventIDTranslation = -15;
        //case icHeartBeat = -21;
        case icSetNoDelay:
          //fDebugStream->print(" icSetNoDelay ");
          break;
        default:
          fDebugStream->print(" >> other-command(");
          fDebugStream->print(aCommand);
          fDebugStream->print(") ");
          fDebugStream->println();
          break;
      }
    }
    return res;
  };
  
  void HandleSocketState(EthernetClient &aSocket)
  {
    if (getConnected())
    {
      Serial.println("disconnected");
      aSocket.stop();  
    }
    if (aSocket.connect(imbHub, imbPort))
    {
      Serial.println("connected");
      // initialize imb
      //SignalConnect(modelName, modelID);
      //SignalSubscribe(eiTestArduino1, "UST.TestArduino1");
      //SignalPublish(eiTestArduino2, "UST.TestArduino2");
    }
    else
    {
      // waiting for a connection to be established to hub
      Serial.print(".");
    }
  };
  
  void SendBasicMessage()
  {
    // prepare payload and read analog port values
    int payloadSize = 2; // 16 digital ports in 2 bytes
    int analogPortValue[6];
    for (int i=0; i<6; i++)
    {
      analogPortValue[i] = analogRead(i);
      payloadSize += Prepare(analogPortValue[i]);
    }
    // build payload
    byte payload[payloadSize];
    byte* payloadPtr = payload;
    *(payloadPtr++) = PIND; // digital pins 0-7
    *(payloadPtr++) = PINB; // digital pins 8-13
    for (int i=0; i<6; i++)
      BufferWrite(payloadPtr, analogPortValue[i]);
    // signal event
    SignalEvent(eiTestArduino2, payload, payloadSize);
  };
  
  unsigned long lastSignalMillis;
  unsigned long lastHeartBeatMillis;
  
  static const unsigned long deltaSignalMillis = 30000;
  static const unsigned long deltaHeartBeatMillis = 5000;

  
};

IMBsocket imb(modelName, modelID, &socket, &Serial); // hookup "socket" to imb, Serial for debugging

void setup() {
  // prepare ports
  DDRD = DDRD & B00010111; // set pins 2,4 as input, 1,2 are serial
  //DDRB = DDRB & B00000011; // set pins 8-9 as input, 13 is led and 14,15 are not on simple pin rest network?
  // open serial communications
  Serial.begin(9600);
  // prepare imb socket
  if (Ethernet.begin(mac) != 0) {
    Serial.print("local ip ");  
    Serial.println(Ethernet.localIP());
  }
  else
    Serial.println("## no dchp address -> check connection and manual reset");
}

void loop() {
  if (socket.connected())
  {
    if (socket.available())
      imb.HandlePacket();
    // send status
    imb.SendBasicMessage();
  }
  else
    imb.HandleSocketState(socket);
  // read control commands from serial
  if (Serial && Serial.available())
  {
    char c = Serial.read();
    switch (c)
    {
      case 'c':
      case 'C':
        imb.Close();
        break;
      case 'q':
      case 'Q':
        imb.Close();
        Serial.println(">> entering endless loop till reset");
        // endless loop till reset
        while (true) delay(1000);
        break;
      case 's':
      case 'S':
        Serial.print(imb.getEventsSend());
        Serial.print("/");
        Serial.print(imb.getEventsReceived());
        Serial.print("/");
        Serial.print(imb.getCommandsReceived());
        Serial.println();
        break;
      case '?':
        // show help
        Serial.println("Options");
        Serial.println("   ? for help");
        Serial.println("   S stats");
        Serial.println("   Q to close and quit (wait for reset)");
        Serial.println("   C close connection");
        Serial.println();
        break;
    }
  }
  // rest of processsing
}

