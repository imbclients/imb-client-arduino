#ifndef Imb_h
#define Imb_h

#include <Arduino.h>
#include <Stream.h>

// imb protocol definitions
const byte imbMagic = 0xFE;
const int imbMinimumPacketSize = 16;
const unsigned int INVALID_EVENT_ID = (unsigned int)-1;

// imb commands
const int icConnect = -1;
const int icClose = -2;
const int icSubscribe = -11;
const int icPublish = -12;
const int icUnsubscribe = -13;
const int icUnpublish = -14;
const int icSetEventIDTranslation = -15;
const int icHeartBeat = -21;
const int icSetNoDelay = -31;

// imb states
const unsigned int icsUninitialized=0;
const unsigned int icsInitialized=1;
const unsigned int icsClient=2;
const unsigned int icsHub=3;
const unsigned int icsEnded=4;
const unsigned int icsTimer=10;
// room for extensions ..
const unsigned int icsGateway=100;
const unsigned int icsGatewayClient=101;
const unsigned int icsGatewayServer=102;

// debug/output tools
void printHexChar(Stream &aStream, byte b);
void printHex(Stream &aStream, byte b);
void printGUIDCompact(Stream &aStream, byte* aGUID);
void printHexEscapes(Stream &aStream, byte* aPayload, int aPayloadSize);

class IMB
{
public:
  IMB();
private:
  // event translation
  unsigned int* fRemoteToLocalEventID;
  unsigned int fRemoteToLocalEventIDLength;
  void AddOrSetRemoteToLocalEventID(unsigned int aRemoteEventID, unsigned int aLocalEventID);
  unsigned int TranslateEventID(unsigned int aRemoteEventID);
  // connect info retrieved 
  byte uniqueClientID[16];
  byte hubID[16];
  // stats
  int fCommandsReceived;
  int fCommandsSend;
  int fEventsReceived;
  int fEventsSend;
public:
  int getEventsReceived() { return fEventsReceived; };
  int getEventsSend() { return fEventsSend; };
  int getCommandsReceived() { return fCommandsReceived; };
public:
  int Read(Stream &aStream, unsigned int &aValue);
  int Read(Stream &aStream, int &aValue);
  int Read(Stream &aStream, float &aValue);
  int Read(Stream &aStream, String &aValue);

  int BufferRead(byte* &aBuffer, unsigned int &aValue);
  int BufferRead(byte* &aBuffer, int &aValue);
  int BufferRead(byte* &aBuffer, float &aValue);
  int BufferRead(byte* &aBuffer, String& aValue);
  
  int SkipBytes(Stream &aStream, int aByteCount);
  
  int Prepare(unsigned int aValue);
  int Prepare(int aValue);
  int Prepare(float aValue);
  int Prepare(const String &aValue);
  
  int BufferWrite(byte* &aBuffer, unsigned int aValue);
  int BufferWrite(byte* &aBuffer, int aValue);
  int BufferWrite(byte* &aBuffer, float aValue);
  int BufferWrite(byte* &aBuffer, const String &aValue);

  int ReadGUID(Stream &aStream, byte* aValue);
  int ReadEventIDFixed(Stream &aStream, unsigned int &aEventIDFixed);
  
  int BufferWriteGUIDEmpty(byte* &aBuffer); // empty GUID all zeros 
  int BufferWriteBytes(byte* &aBuffer, byte* aValue, int aSizeValue);
  void BufferWriteFillingBytes(byte* &aBuffer, int aSizeCommand);
public:
  // signal event/command
  void SignalConnect(Stream &aStream, const String &aModelName, int aModelID);
  void SignalSubscribe(Stream &aStream, unsigned int aEventID, const String &aEventName);
  void SignalPublish(Stream &aStream, unsigned int aEventID, const String &aEventName);
  void SignalUnSubscribe(Stream &aStream, unsigned int aEventID);
  void SignalUnPublish(Stream &aStream, unsigned int aEventID);
  void SignalNoDelay(Stream &aStream, bool aNoDelay);
  void SignalClose(Stream &aStream);
  void SignalEvent(Stream &aStream, unsigned int aEventID, byte* aData, int aSizeData);
public:
  // higher level imb command/event handling
  void Close() { HandleCommandClose(); };
  void HandleCommand(Stream &aStream);
  // empty handlers to override when needed
  virtual void HandleEvent(unsigned int aEventID, byte* aEventData, int aEventDataSize) {};
  virtual void HandleCommandSetEventIDTranslation(unsigned int remoteEventID, unsigned int localEventID) {};
  virtual void HandleCommandClose() {};
  virtual void HandleCommandConnect(byte* uniqueClientID, byte* hubID) {};
  virtual int HandleCommandHeartBeat(Stream &aStream, int aCommandPayloadSize) { return 0; };
  virtual void HandleDidNotFindMagic(byte aReceivedByte) {};
  virtual int HandleOtherCommand(int aCommand, Stream &aStream, int aCommandPayloadSize) { return 0; };
};

#endif

