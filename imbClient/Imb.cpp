#include <Stream.h>
#include "Imb.h"

// debug/output tools
void printHexChar(Stream &aStream, byte b)
{
  if (b<10)
    aStream.write('0'+b);
  else
    aStream.write('A'+b-10);
}

void printHex(Stream &aStream, byte b)
{
  printHexChar(aStream, b>>4);
  printHexChar(aStream, b & 0x0F);
}

void printGUIDCompact(Stream &aStream, byte* aGUID)
{
  // order of GUID 4-2-2-2-1-1-1-1-1-1-1-1
  printHex(aStream, aGUID[3]); printHex(aStream, aGUID[2]); printHex(aStream, aGUID[1]); printHex(aStream, aGUID[0]);
  printHex(aStream, aGUID[5]); printHex(aStream, aGUID[4]);
  printHex(aStream, aGUID[7]); printHex(aStream, aGUID[6]);
  printHex(aStream, aGUID[8]);
  printHex(aStream, aGUID[9]);
  printHex(aStream, aGUID[10]);
  printHex(aStream, aGUID[11]);
  printHex(aStream, aGUID[12]);
  printHex(aStream, aGUID[13]);
  printHex(aStream, aGUID[14]);
  printHex(aStream, aGUID[15]);
}

void printHexEscapes(Stream &aStream, byte* aPayload, int aPayloadSize)
{
  for (int i=0; i<aPayloadSize; i++)
  {
    if (aPayload[i]<32 || aPayload[i]>127)
    {
      aStream.print("<");
      printHex(aStream, aPayload[i]);
      aStream.print(">");
    }
    else
      aStream.print((char)aPayload[i]);
  }
}

// IMB class

IMB::IMB()
{
  fRemoteToLocalEventID = 0;
  fRemoteToLocalEventIDLength = 0;
  // stats
  fCommandsReceived = 0;
  fCommandsSend = 0;
  fEventsReceived = 0;
  fEventsSend = 0;
  // clear guids
  for (int i=0; i<16; i++)
  {
    uniqueClientID[i] = 0;
    hubID[i] = 0;
  }
}

void IMB::AddOrSetRemoteToLocalEventID(unsigned int aRemoteEventID, unsigned int aLocalEventID)
{
  if (aRemoteEventID>=fRemoteToLocalEventIDLength)
  {
    // create room for new remote event id
     unsigned int* newRemoteToLocalEventID = new unsigned int[aRemoteEventID+1];
    // copy old entries
    for (int i=0; i<fRemoteToLocalEventIDLength; i++)
      newRemoteToLocalEventID[i] = fRemoteToLocalEventID[i];
    // init new entries
    for (int i=fRemoteToLocalEventIDLength; i<aRemoteEventID; i++)
      newRemoteToLocalEventID[i] = INVALID_EVENT_ID;
    fRemoteToLocalEventIDLength = aRemoteEventID+1;
    delete(fRemoteToLocalEventID);
    fRemoteToLocalEventID = newRemoteToLocalEventID;
  }
  fRemoteToLocalEventID[aRemoteEventID] = aLocalEventID;
}

unsigned int IMB::TranslateEventID(unsigned int aRemoteEventID)
{
  if (0<=aRemoteEventID && aRemoteEventID<fRemoteToLocalEventIDLength)
    return fRemoteToLocalEventID[aRemoteEventID];
  else
    return INVALID_EVENT_ID;
}

int IMB::ReadEventIDFixed(Stream &aStream, unsigned int &aEventIDFixed)
{
  return aStream.readBytes((char *)&aEventIDFixed, 2);
}

int IMB::Read(Stream &aStream, unsigned int &aValue)
{
  int res=1;
  byte b=aStream.read();
  if (b<128)
    aValue = b;
  else
  {
    unsigned int newValue;
    res += Read(aStream, newValue);
    aValue = (newValue << 7) || (b & 0x7F); 
  }
  return res;
}

int IMB::Read(Stream &aStream, int &aValue)
{
  unsigned int uValue;
  int res=Read(aStream, uValue);
  if (uValue & 1)
    aValue = -(uValue >> 1);
  else
    aValue = uValue >> 1;
  return res;
}

int IMB::Read(Stream &aStream, float &aValue)
{
  char* valuePtr = (char*)&aValue;
  return aStream.readBytes(valuePtr, sizeof(float));
}

int IMB::Read(Stream &aStream, String &aValue)
{
  unsigned int len;
  int res = Read(aStream, len);
  char value[len+1];
  res += aStream.readBytes(value, len);
  aValue[len] = 0;
  return res;
}

int IMB::ReadGUID(Stream &aStream, byte* aValue)
{
  unsigned int len;
  int res = Read(aStream, len);
  res += aStream.readBytes((char*)aValue, len);
  return res;
}

int IMB::BufferRead(byte* &aBuffer, unsigned int &aValue)
{
  int res=1;
  byte b=*(aBuffer++);
  if (b<128)
    aValue = b;
  else
  {
    unsigned int newValue;
    res += BufferRead(aBuffer, newValue);
    aValue = (newValue << 7) || (b & 0x7F); 
  }
  return res;
}

int IMB::BufferRead(byte* &aBuffer, int &aValue)
{
  unsigned int uValue;
  int res=BufferRead(aBuffer, uValue);
  if (uValue & 1)
    aValue = -(uValue >> 1);
  else
    aValue = uValue >> 1;
  return res;
}

int IMB::BufferRead(byte* &aBuffer, float &aValue)
{
  char* valuePtr = (char*)&aValue;
  int len = sizeof(float);
  for (int i=0; i<len; i++)
    *(valuePtr++) = *(aBuffer++);
  return len;
}

int IMB::BufferRead(byte* &aBuffer, String& aValue)
{
  unsigned int len;
  int res = BufferRead(aBuffer, len);
  char value[len+1];
  for (int i=0; i<len;i++)
    value[i] = *(aBuffer++);
  value[len] = 0;
  aValue = String(value);
  return res+len;
}

int IMB::SkipBytes(Stream &aStream, int aByteCount)
{
  if (aByteCount>0)
  {
    byte buffer[aByteCount];
    int res = aStream.readBytes((char*)buffer, aByteCount);
//    if (Serial)
//    {
//      Serial.print(">> skip (");
//      Serial.print(aByteCount);
//      Serial.print(") ");
//      printHexEscapes(Serial, buffer, res);
//      Serial.println();
//    }
    return res;
  }
  else
    return 0;
}

void IMB::HandleCommand(Stream &aStream)
{
  byte magic=aStream.read();
  if (magic==imbMagic)
  {
    int sizePayload;
    // number of bytes for header and size
    int sizeHeader=1+Read(aStream, sizePayload); 
    if (sizePayload>0)
    {
      unsigned int remoteEventIDFixed;
      int readPayload = ReadEventIDFixed(aStream, remoteEventIDFixed);
      unsigned int localEventID = TranslateEventID(remoteEventIDFixed);
//      if (Serial)
//      {
//        Serial.print("event ");
//        Serial.print(remoteEventIDFixed);
//        Serial.print(" -> ");
//        Serial.print(localEventID);
//        Serial.print(": ");
//      }
      int eventPayloadSize = sizePayload-readPayload;
      byte eventPayload[eventPayloadSize];
      int res = aStream.readBytes((char*)eventPayload, eventPayloadSize);
      HandleEvent(localEventID, eventPayload, res);
      fEventsReceived++;
    }
    else
    {
      sizePayload = -sizePayload;
      int command;
      int readPayload = Read(aStream, command);
      // decoding storage
      unsigned int localEventID;
      unsigned int remoteEventID;
      String dummyModelName;
      int dummyModelID;
      unsigned int dummyReconnectable;
      unsigned int dummyState;
      // handle command
      switch (command)
      {
        case icSetEventIDTranslation:
          readPayload += Read(aStream, localEventID);
          readPayload += Read(aStream, remoteEventID);
          AddOrSetRemoteToLocalEventID(remoteEventID, localEventID);
          HandleCommandSetEventIDTranslation(remoteEventID, localEventID);
          break;
        case icClose:
          HandleCommandClose();
          break;
        case icConnect:
          readPayload += Read(aStream, dummyModelName);
          readPayload += Read(aStream, dummyModelID);
          readPayload += ReadGUID(aStream, uniqueClientID);
          readPayload += ReadGUID(aStream, hubID);
          readPayload += Read(aStream, dummyReconnectable);
          readPayload += Read(aStream, dummyState);
          HandleCommandConnect(uniqueClientID, hubID);
          break;
        case icHeartBeat:
          readPayload += HandleCommandHeartBeat(aStream, sizePayload-readPayload);
          break;
        default:
          readPayload += HandleOtherCommand(command, aStream, sizePayload-readPayload);
          break;
      }
//      Serial.print("rest (");
//      Serial.print(command);
//      Serial.print(") ");
//      Serial.print(sizePayload);
//      Serial.print(" - ");
//      Serial.println(readPayload);
      readPayload += SkipBytes(aStream, sizePayload-readPayload);
      fCommandsReceived++;
    }
    // read rest of command if size is to low
//    Serial.print("min packet size ");
//    Serial.print(imbMinimumPacketSize);
//    Serial.print(" - ");
//    Serial.println(sizeHeader+sizePayload);
    SkipBytes(aStream, imbMinimumPacketSize-(sizeHeader+sizePayload));
  }
  else
    HandleDidNotFindMagic(magic);
}

int IMB::Prepare(unsigned int aValue)
{
  if (aValue<128)
    return 1;
  else
    return 1+Prepare((unsigned int)(aValue >> 7));
}

int IMB::Prepare(int aValue)
{
  if (aValue<0)
    return Prepare((unsigned int)(((-aValue) << 1) | 1));
  else
    return Prepare((unsigned int)(aValue << 1));
}

int IMB::Prepare(float aValue)
{
  return sizeof(float);
}

int IMB::Prepare(const String &aValue)
{
  unsigned int len = aValue.length();
  return Prepare(len)+len;
}

int IMB::BufferWrite(byte* &aBuffer, unsigned int aValue)
{
  if (aValue<128)
  {
    *(aBuffer++)= (byte)aValue;
    return 1;
  }
  else
  {
    *(aBuffer++)= (byte)(aValue & 0x7F) | 0x80;
    return BufferWrite(aBuffer, (unsigned int)(aValue >> 7))+1;
  }
}

int IMB::BufferWrite(byte* &aBuffer, float aValue)
{
  int len = sizeof(float);
  byte* valuePtr = (byte*)&aValue;
  for (int i=0; i<len; i++)
    aBuffer[i] = valuePtr[i];
  aBuffer += len;
  return len;
}

int IMB::BufferWrite(byte* &aBuffer, int aValue)
{
  if (aValue<0)
    return BufferWrite(aBuffer, (unsigned int)((-aValue) << 1) | 1);
  else
    return BufferWrite(aBuffer, (unsigned int)(aValue << 1));
}

int IMB::BufferWrite(byte* &aBuffer, const String &aValue)
{
  unsigned int len = aValue.length();
  int res = BufferWrite(aBuffer, len);
  for (int i=0; i<len; i++)
    *(aBuffer++) = (byte)aValue[i];
  res += len;
  return res; 
}

int IMB::BufferWriteGUIDEmpty(byte* &aBuffer)
{
  unsigned int len = 16;
  int res = BufferWrite(aBuffer, len);
  for (int i=0; i<len; i++)
    *(aBuffer++) = 0;
  res += len;
  return res;
}

int IMB::BufferWriteBytes(byte* &aBuffer, byte* aValue, int aSizeValue)
{
  for (int i=0; i<aSizeValue; i++)
    *(aBuffer++) = *(aValue++);
  return aSizeValue;
}

// signal functions

void IMB::SignalConnect(Stream &aStream, const String &aModelName, int aModelID)
{
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icsClient);
  sizePayload += Prepare((unsigned int)0); // fReconnectable
  sizePayload += 17; // fHubID
  sizePayload += 17; // uniqueClientID
  sizePayload += Prepare(aModelID);
  sizePayload += Prepare(aModelName);
  sizePayload += Prepare(icConnect);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; // aStream.write((byte)imbMagic);
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icConnect);
  BufferWrite(bufferPtr, aModelName);
  BufferWrite(bufferPtr, aModelID);
  BufferWriteGUIDEmpty(bufferPtr);
  BufferWriteGUIDEmpty(bufferPtr);
  BufferWrite(bufferPtr, (unsigned int)0);
  BufferWrite(bufferPtr, icsClient);
  aStream.write(buffer, sizeCommand); 
  fCommandsSend++;
}

void IMB::SignalSubscribe(Stream &aStream, unsigned int aEventID, const String &aEventName)
{
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icSubscribe);
  sizePayload += Prepare(aEventID);
  sizePayload += Prepare(aEventName);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; // aStream.write((byte)imbMagic);
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icSubscribe);
  BufferWrite(bufferPtr, aEventID);
  BufferWrite(bufferPtr, aEventName);
  aStream.write(buffer, sizeCommand);
  fCommandsSend++;
}

void IMB::SignalPublish(Stream &aStream, unsigned int aEventID, const String &aEventName)
{
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icPublish);
  sizePayload += Prepare(aEventID);
  sizePayload += Prepare(aEventName);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; // aStream.write((byte)imbMagic);
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icPublish);
  BufferWrite(bufferPtr, aEventID);
  BufferWrite(bufferPtr, aEventName);
  aStream.write(buffer, sizeCommand);
  fCommandsSend++;
}

void IMB::SignalUnSubscribe(Stream &aStream, unsigned int aEventID)
{
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icUnsubscribe);
  sizePayload += Prepare(aEventID);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; // aStream.write((byte)imbMagic);
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icUnsubscribe);
  BufferWrite(bufferPtr, aEventID);
  aStream.write(buffer, sizeCommand);
  fCommandsSend++;
}

void IMB::SignalUnPublish(Stream &aStream, unsigned int aEventID)
{
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icUnpublish);
  sizePayload += Prepare(aEventID);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; // aStream.write((byte)imbMagic);
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icUnpublish);
  BufferWrite(bufferPtr, aEventID);
  aStream.write(buffer, sizeCommand);
  fCommandsSend++;
}

void IMB::SignalNoDelay(Stream &aStream, bool aNoDelay)
{
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare((unsigned int)aNoDelay);
  sizePayload += Prepare(icSetNoDelay);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; // aStream.write((byte)imbMagic);
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icSetNoDelay);
  BufferWrite(bufferPtr, (unsigned int)aNoDelay);
  aStream.write(buffer, sizeCommand);
  fCommandsSend++;
}

void IMB::SignalClose(Stream &aStream)
{
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icClose);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; // aStream.write((byte)imbMagic);
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icClose);
  aStream.write(buffer, sizeCommand);
  fCommandsSend++;
}

void IMB::SignalEvent(Stream &aStream, unsigned int aEventID, byte* aData, int aSizeData)
{
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += 2; // event id (fixed)
  sizePayload += aSizeData;
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; // aStream.write((byte)imbMagic);
  BufferWrite(bufferPtr, sizePayload);
  BufferWriteBytes(bufferPtr, (byte*)&aEventID, 2);
  BufferWriteBytes(bufferPtr, aData, aSizeData);
  aStream.write(buffer, sizeCommand); 
  fEventsSend++;
}

