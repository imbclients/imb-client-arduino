#include <SPI.h>
#include <Ethernet.h>
#include "Imb.h"

// configurable settings

// connection to hub (TCP)
IPAddress imbHub(192,168,1,100);
int imbPort = 4000;

// model name and id
int modelID = 4;
String modelName = "Arduino";

// ethernet MAC address
byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };

// event id's
const int eiTestArduino1 = 0;
const int eiTestArduino2 = 1;

// keep track of connection state
bool imbConnected = false;

// TCP socket
EthernetClient socket;

// derive class to override HandleEvent
class MyIMB:public IMB
{
public:
  // override handler functions
  virtual void HandleEvent(unsigned int aEventID, byte* aEventData, int aEventDataSize)
  {
    // handle event
    String s;
    int i;
    float f;
    BufferRead(aEventData, s);
    BufferRead(aEventData, i);
    BufferRead(aEventData, f);
  };
  virtual void HandleCommandSetEventIDTranslation(unsigned int remoteEventID, unsigned int localEventID) 
  {
    if (Serial)
    {
      Serial.print("icSetEventIDTranslation: ");
      Serial.print(localEventID);
      Serial.print(" -> ");
      Serial.println(remoteEventID);
    }
  };
  virtual void HandleCommandClose() 
  {
    if (socket.connected())
    {
      SignalClose(socket);
      socket.stop();
      imbConnected = false;
    }
    if (Serial)
      Serial.println("icClose ");
  };
  virtual void HandleCommandConnect(byte* uniqueClientID, byte* hubID) 
  {
    if (Serial)
    {
      Serial.print("icConnect uniqueClientID ");
      printGUIDCompact(Serial, uniqueClientID);
      Serial.print(" hubID ");
      printGUIDCompact(Serial, hubID);
      Serial.println();
    }
  };
  virtual int HandleCommandHeartBeat(Stream &aStream, int aCommandPayloadSize) 
  {
    int res=0;
    if (Serial)
    {
      Serial.print("icHeartBeat ");
      byte payload[aCommandPayloadSize];
      res = aStream.readBytes((char*)payload, aCommandPayloadSize);
      printHexEscapes(Serial, payload, aCommandPayloadSize);
      Serial.println();
    }
    return res;
  };
  virtual void HandleDidNotFindMagic(byte aReceivedByte) 
  {
    if (Serial)
    {
      Serial.print("## did not find magic ");
      printHex(Serial, aReceivedByte);
      Serial.println();
    }
  };
  virtual int HandleOtherCommand(int aCommand, Stream &aStream, int aCommandPayloadSize) 
  {
    int res=0;
    if (Serial)
    {
      switch (aCommand)
      {
        //case icConnect = -1;
        //case icClose = -2;
        case icSubscribe:
          Serial.println(">> other: icSubscribe");
          break;
        case icPublish:
          Serial.println(">> other: icPublish");
          break;
        case icUnsubscribe:
          Serial.println(">> other: icUnSubscribe");
          break;
        case icUnpublish:
          Serial.println(">> other: icUnpublish");
          break;
        //case icSetEventIDTranslation = -15;
        //case icHeartBeat = -21;
        case icSetNoDelay:
          Serial.println(">> other: icSetNoDelay");
          break;
        default:
          Serial.print("## other: unknown (");
          Serial.print(aCommand);
          Serial.println(")");
          break;
      }
    }
    return res;
  };
  
  // setup etc. 
  void Setup()
  {
    if (Ethernet.begin(mac) != 0) {
    if (Serial)
    {
      Serial.print("local ip ");  
      Serial.println(Ethernet.localIP());
    }
  }
  else
    if (Serial) 
      Serial.println("## no dchp address -> check connection and manual reset");
  };
  
  void HandleSocketState(EthernetClient &aSocket)
  {
    if (imbConnected)
    {
      if (Serial)
        Serial.println("disconnected");
      aSocket.stop();  
      imbConnected = false;
    }
    if (aSocket.connect(imbHub, imbPort))
    {
      imbConnected = true;
      if (Serial)
      {
        Serial.println("connected");
      }
      // initialize imb
      SignalConnect(aSocket, modelName, modelID);
      SignalSubscribe(aSocket, eiTestArduino1, String("UST.TestArduino1"));
      SignalPublish(aSocket, eiTestArduino2, String("UST.TestArduino2"));
    }
    else
    {
      // waiting for a connection to be established to hub
      if (Serial)
        Serial.print(".");
    }
  };
  
  void SendBasicMessage(Stream& aStream)
  {
    int payloadSize = 2; // 16 digital ports
    int analogPortValue[6];
    for (int i=0; i<6; i++)
    {
      analogPortValue[i] = analogRead(i);
      payloadSize += Prepare(analogPortValue[i]);
    }
    byte payload[payloadSize];
    byte* payloadPtr = payload;
    *(payloadPtr++) = PIND; // digital pins 0-7
    *(payloadPtr++) = PINB; // digital pins 8-13
    for (int i=0; i<6; i++)
      BufferWrite(payloadPtr, analogPortValue[i]);
    SignalEvent(aStream, eiTestArduino2, payload, payloadSize);
  };
} imb;

void setup() {
  // prepare ports
  DDRD = DDRD & B00010111; // set pins 2,4 as input, 1,2 are serial
  //DDRB = DDRB & B00000011; // set pins 8-9 as input, 13 is led and 14,15 are not on simple pin rest network?
  // open serial communications
  Serial.begin(9600);
  // prepeare imb
  imb.Setup();
}

void loop() {
  if (socket.connected())
  {
    if (socket.available())
      imb.HandleCommand(socket);
    // send status
    imb.SendBasicMessage(socket);
  }
  else
    imb.HandleSocketState(socket);
  // read control commands from serial
  if (Serial && Serial.available())
  {
    char c = Serial.read();
    switch (c)
    {
      case 'c':
      case 'C':
        imb.Close();
        break;
      case 'q':
      case 'Q':
        imb.Close();
        Serial.println(">> entering endless loop till reset");
        // endless loop till reset
        while (true) delay(1000);
        break;
      case 's':
      case 'S':
        Serial.print(imb.getEventsSend());
        Serial.print("/");
        Serial.print(imb.getEventsReceived());
        Serial.print("/");
        Serial.print(imb.getCommandsReceived());
        Serial.println();
        break;
      case '?':
        // show help
        Serial.println("Options");
        Serial.println("   ? for help");
        Serial.println("   S stats");
        Serial.println("   Q to close and quit (wait for reset)");
        Serial.println("   C close connection");
        Serial.println();
        break;
    }
  }
  // rest of processsing
}

