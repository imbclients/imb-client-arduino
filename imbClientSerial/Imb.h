#ifndef Imb_h
#define Imb_h

#include <Arduino.h>
#include <Stream.h>

// imb protocol definitions
const byte imbMagic = 0xFE;
const int imbMinimumPacketSize = 16;
const unsigned int INVALID_EVENT_ID = (unsigned int)-1;

// imb commands
const int icConnect = -1;
const int icClose = -2;
const int icReconnect = -3;
const int icHeartBeat = -4;
const int icSubscribe = -11;
const int icPublish = -12;
const int icUnsubscribe = -13;
const int icUnpublish = -14;
const int icSetEventIDTranslation = -15;
const int icSetNoDelay = -21;
const int icSetState = -22;

// imb states
const unsigned int icsUninitialized=0;
const unsigned int icsInitialized=1;
const unsigned int icsClient=2;
const unsigned int icsHub=3;
const unsigned int icsEnded=4;
const unsigned int icsTimer=10;
// room for extensions ..
const unsigned int icsGateway=100;
const unsigned int icsGatewayClient=101;
const unsigned int icsGatewayServer=102;

void printGUIDCompact(Stream &aStream, byte* aGUID);

class IMB
{
public:
  IMB(const char* aModelName, int aModelID, bool aReconectable, Stream* aStream, bool aUseCheckSum, Stream* aDebugStream=0);
protected:
  const char* fModelName;
  int fModelID;
  bool fReconectable;
  Stream* fStream;
  Stream* fDebugStream;
  bool fConnected;
  bool fWritingEnabled;
  // event translation
  unsigned int* fRemoteToLocalEventID;
  unsigned int fRemoteToLocalEventIDLength;
  void AddOrSetRemoteToLocalEventID(unsigned int aRemoteEventID, unsigned int aLocalEventID);
  unsigned int TranslateEventID(unsigned int aRemoteEventID);
  // connect info retrieved 
  byte uniqueClientID[16];
  byte hubID[16];
  // stats
  int fCommandsReceived;
  int fCommandsSend;
  int fEventsReceived;
  int fEventsSend;
  int fErr;
  int fMissedMagic;
  int fCheckSum;
  bool fUseCheckSum;
  void FlushCommand(byte* aBuffer, int aSize);
  bool testCheckSum(int aFillerBytes);
public:
  int getCommandsReceived() { return fCommandsReceived; };
  int getCommandsSend() { return fCommandsSend; };
  int getEventsReceived() { return fEventsReceived; };
  int getEventsSend() { return fEventsSend; };
  int getErr() { return fErr; };
  int getMissedMagic() { return fMissedMagic; };
  bool getWritingEnabled() { return fWritingEnabled; };
  virtual bool getConnected() { return fConnected; };
  void setConnected(bool aValue) { fConnected = aValue; fWritingEnabled = false; };
public:
  int Read(unsigned int &aValue);
  int Read(int &aValue);
  int Read(float &aValue);
  int Read(byte* aValue, int aValueSize);
  int Read(String &aValue);

  int SkipBytes(int aByteCount);
  
  int BufferRead(byte* &aBuffer, unsigned int &aValue);
  int BufferRead(byte* &aBuffer, int &aValue);
  int BufferRead(byte* &aBuffer, float &aValue);
  int BufferRead(byte* &aBuffer, byte* aValue, int aValueSize);
  int BufferRead(byte* &aBuffer, String& aValue);
  
  int Prepare(unsigned int aValue);
  int Prepare(int aValue);
  int Prepare(float aValue);
  int Prepare(char* aValue);
  
  int BufferWrite(byte* &aBuffer, unsigned int aValue);
  int BufferWrite(byte* &aBuffer, int aValue);
  int BufferWrite(byte* &aBuffer, float aValue);
  int BufferWrite(byte* &aBuffer, byte* aValue, int aValueSize);
  int BufferWrite(byte* &aBuffer, char* aValue);

  int ReadGUID(byte* aValue);
  int ReadEventIDFixed(unsigned int &aEventIDFixed);
  
  int BufferWriteGUIDEmpty(byte* &aBuffer); // empty GUID all zeros 
  void BufferWriteFillingBytes(byte* &aBuffer, int aSizeCommand);
public:
  // signal event/command
  void SignalConnect(const char* aModelName, int aModelID, bool aReconnectable);
  void SignalSubscribe(unsigned int aEventID, const char* aEventName);
  void SignalPublish(unsigned int aEventID, const char* aEventName);
  void SignalUnSubscribe(unsigned int aEventID);
  void SignalUnPublish(unsigned int aEventID);
  void SignalNoDelay(bool aNoDelay);
  void SignalClose();
  void SignalHeartBeat();
  void SignalEvent(unsigned int aEventID, byte* aData, int aSizeData);
public:
  // higher level imb command/event handling
  void Close() { HandleCommandClose(); };
  void HandleCommand();
  // empty handlers to override when needed
  virtual void HandleEvent(unsigned int aEventID, byte* aEventData, int aEventDataSize) {};
  virtual void HandleCommandSetEventIDTranslation(unsigned int remoteEventID, unsigned int localEventID) {};
  virtual void HandleCommandClose() {};
  virtual void HandleCommandConnect(byte* uniqueClientID, byte* hubID) {};
  virtual int HandleCommandHeartBeat(int aCommandPayloadSize);
  virtual void HandleDidNotFindMagic(byte aReceivedByte) {};
  virtual int HandleOtherCommand(int aCommand, int aCommandPayloadSize) { return 0; };
  virtual void HandleInit(byte* aHeader, int aHeaderSize)=0;
  virtual void HandleCheckSumError() {};
};

#endif

