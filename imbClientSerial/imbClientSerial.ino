#include "Imb.h"
#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

char checkKey()
{
  int keyPress = analogRead(0);
  if(keyPress < 65)
    return 'R';
  else if(keyPress < 221)
    return 'U';
  else if(keyPress < 395)
    return 'D';
  else if(keyPress < 602)
    return 'L';
  else if(keyPress < 873)
    return 'S';
  else
    return 0;
}

// configurable settings

// model name and id
const int modelID = 4;
const char modelName[] = "Arduino";

// local event id's
const int eiTestArduino1 = 0;
const int eiTestArduino2 = 1;

// derive class to override HandleEvent
class IMBserial:public IMB
{
public:
  IMBserial(const char* aModelName, int aModelID, Stream* aStream, Stream* aDebugStream=0) : IMB(aModelName, aModelID, true, aStream, true, aDebugStream) 
  {
    lastSignalMillis = 0;
    lastHeartBeatMillis = 0;
  };
public:
  // override handler functions
  virtual void HandleEvent(unsigned int aEventID, byte* aEventData, int aEventDataSize)
  {
    // handle event
    String s;
    int i;
    float f;
    BufferRead(aEventData, s);
    BufferRead(aEventData, i);
    BufferRead(aEventData, f);
    // process values
  };
  virtual void HandleCommandClose() 
  {
    if (fDebugStream)
      fDebugStream->println(" icClose");
  };
  virtual void HandleCommandConnect(byte* uniqueClientID, byte* hubID) 
  {
    if (fDebugStream)
    {
      fDebugStream->print(" icConnect(");
      printGUIDCompact(*fDebugStream, uniqueClientID);
      fDebugStream->print(",");
      printGUIDCompact(*fDebugStream, hubID);
      fDebugStream->print(") ");
      fDebugStream->println();
    }
  };
  virtual void HandleInit(byte* aHeader, int aHeaderSize)
  {
    SignalSubscribe(eiTestArduino1, "UST.TestArduino1");
    SignalPublish(eiTestArduino2, "UST.TestArduino2");
  };
  virtual void HandleDidNotFindMagic(byte aReceivedByte) 
  {
    if (fDebugStream)
    {
      fDebugStream->print("##M(");
      fDebugStream->print(aReceivedByte);
      if (aReceivedByte>=32 && aReceivedByte<128)
      {
        fDebugStream->print(":");
        fDebugStream->print(char(aReceivedByte));
      }
      fDebugStream->println(")");
    }
  };
  virtual int HandleOtherCommand(int aCommand, int aCommandPayloadSize) 
  {
    int res=0;
    if (fDebugStream)
    {
      switch (aCommand)
      {
        //case icConnect = -1;
        //case icClose = -2;
        case icSubscribe:
          //fDebugStream->print(" icSubscribe ");
          break;
        case icPublish:
          //fDebugStream->print(" icPublish ");
          break;
        case icUnsubscribe:
          //fDebugStream->print(" icUnsubscribe ");
          break;
        case icUnpublish:
          //fDebugStream->print(" icUnpublish ");
          break;
        //case icSetEventIDTranslation = -15;
        //case icHeartBeat = -21;
        case icSetNoDelay:
          //fDebugStream->print(" icSetNoDelay ");
          break;
        default:
          fDebugStream->print(" >> other-command(");
          fDebugStream->print(aCommand);
          fDebugStream->print(") ");
          fDebugStream->println();
          break;
      }
    }
    return res;
  };
  
  void SendBasicMessage()
  {
    // prepare payload and read analog port values
    int payloadSize = 2; // 16 digital ports in 2 bytes
    int analogPortValue[6];
    for (int i=0; i<6; i++)
    {
      analogPortValue[i] = analogRead(i);
      payloadSize += Prepare(analogPortValue[i]);
    }
    // build payload
    byte payload[payloadSize];
    byte* payloadPtr = payload;
    *(payloadPtr++) = PIND; // digital pins 0-7
    *(payloadPtr++) = PINB; // digital pins 8-13
    for (int i=0; i<6; i++)
      BufferWrite(payloadPtr, analogPortValue[i]);
    // signal event
    SignalEvent(eiTestArduino2, payload, payloadSize);
  };
  
  unsigned long lastSignalMillis;
  unsigned long lastHeartBeatMillis;
  
  static const unsigned long deltaSignalMillis = 30000;
  static const unsigned long deltaHeartBeatMillis = 5000;
  
  void Loop()
  {
    if (getConnected())
    {  
      if (fStream->available())
        HandleCommand();
      else
      {
        if (getWritingEnabled())
        {
          unsigned long currentMillis = millis();
          if (lastSignalMillis+deltaSignalMillis<currentMillis || lastSignalMillis>currentMillis)
          {
            lastSignalMillis=currentMillis;
            lastHeartBeatMillis=currentMillis;
            SendBasicMessage();
          }
          else
          {
            if (lastHeartBeatMillis+deltaHeartBeatMillis<currentMillis || lastHeartBeatMillis>currentMillis)
            {
              lastHeartBeatMillis=currentMillis;
              SignalHeartBeat();
            }
          }
        }
      }
    }
    else
    {
      // try to connect
      int b=fStream->read();
      if (b>=0)
      {
        if (b==0xFD)
        {
          if (fDebugStream)
            fDebugStream->print("trying to connect..");
          // confirm connection
          fStream->write(0xFD);
          fStream->flush();
          while (b<0 || b==0xFD)
            b=fStream->read();
          // test for connected
          setConnected(b==0xFC);
          if (fDebugStream)
          {
            if (getConnected())
              fDebugStream->println(": confirmed");
            else
              fDebugStream->println(" ## aborted");
          }
        }
      }
    }
  };
};

IMBserial imb(modelName, modelID, &Serial, &Serial1); // hookup "Serial" to imb, Serial1 for debugging

void setup() {
  // open display
  lcd.begin(16, 2);
  // open serial for imb communication
  Serial.begin(115200);
  // open debug connection
  Serial1.begin(57600);
  // prepare ports
  //DDRD = DDRD & B00010111; // set pins 2,4 as input, 1,2 are serial
  //DDRB = DDRB & B00000011; // set pins 8-9 as input, 13 is led and 14,15 are not on simple pin rest network?
  Serial1.println();
  Serial1.println("setup ready");
}

void loop() 
{
  imb.Loop();

  // check debugging control
  if (Serial1.available())
  {
    int b=Serial1.read();
    if (b>=0)
    {
      if (b=='?')
      {
        Serial1.print("cr=");
        Serial1.print(imb.getCommandsReceived());
        Serial1.print(" cs=");
        Serial1.print(imb.getCommandsSend());
        Serial1.print(" er=");
        Serial1.print(imb.getEventsReceived());
        Serial1.print(" es=");
        Serial1.print(imb.getEventsSend());
        Serial1.print(" err=");
        Serial1.print(imb.getErr());
        Serial1.print(" mm=");
        Serial1.print(imb.getMissedMagic());
        Serial1.println();
      }
      if (b=='f')
      {
        Serial.write(0);
        Serial.flush();
      }
      if (b=='c')
      {
        imb.SignalClose();
        Serial1.println("requested close, waiting for new connection..");
      }
      if (b=='k')
      {
        // kill client by hang
        Serial1.println("requested kill, waiting forever..");
        while (true) ;
      }
    }
  }
  // rest of processsing
}

