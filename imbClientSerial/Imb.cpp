#include <Stream.h>
#include "Imb.h"

//#define DebugToStream

#ifdef DebugToStream
#define DebugOut(a) fDebugStream->print(a)
#define DebugOutLn(a) fDebugStream->println(a)
#else
#define DebugOut(a)
#define DebugOutLn(a)
#endif


// tools

void printHexChar(Stream &aStream, byte b)
{
  if (b<10)
    aStream.write('0'+b);
  else
    aStream.write('A'+b-10);
}

void printHex(Stream &aStream, byte b)
{
  printHexChar(aStream, b>>4);
  printHexChar(aStream, b & 0x0F);
}

void printGUIDCompact(Stream &aStream, byte* aGUID)
{
  // order of GUID 4-2-2-2-1-1-1-1-1-1-1-1
  printHex(aStream, aGUID[3]); printHex(aStream, aGUID[2]); printHex(aStream, aGUID[1]); printHex(aStream, aGUID[0]);
  printHex(aStream, aGUID[5]); printHex(aStream, aGUID[4]);
  printHex(aStream, aGUID[7]); printHex(aStream, aGUID[6]);
  printHex(aStream, aGUID[8]);
  printHex(aStream, aGUID[9]);
  printHex(aStream, aGUID[10]);
  printHex(aStream, aGUID[11]);
  printHex(aStream, aGUID[12]);
  printHex(aStream, aGUID[13]);
  printHex(aStream, aGUID[14]);
  printHex(aStream, aGUID[15]);
}

int checkSum(byte* aBuffer, int aSize)
{
  int res=-1;
  for (int i=0; i<aSize; i++)
    res += aBuffer[i];
  return res;
}

// IMB class

IMB::IMB(const char* aModelName, int aModelID, bool aReconectable, Stream* aStream, bool aUseCheckSum, Stream* aDebugStream) //:  lcd(8, 9, 4, 5, 6, 7) 
{
  fModelName = aModelName;
  fModelID = aModelID;
  fReconectable = aReconectable;
  fStream = aStream;
  fUseCheckSum = aUseCheckSum;
  
  fDebugStream=aDebugStream; 
  
  fConnected = false;;
  fWritingEnabled = false;
  
  fRemoteToLocalEventID = 0;
  fRemoteToLocalEventIDLength = 0;
  // stats
  fCommandsReceived = 0;
  fCommandsSend = 0;
  fEventsReceived = 0;
  fEventsSend = 0;
  fErr = 0;
  fMissedMagic = 0;
  // clear guids
  memset(uniqueClientID, 0, 16);
  memset(hubID, 0, 16);
}

void IMB::AddOrSetRemoteToLocalEventID(unsigned int aRemoteEventID, unsigned int aLocalEventID)
{
  if (aRemoteEventID>=fRemoteToLocalEventIDLength)
  {
    // create room for new remote event id
     unsigned int* newRemoteToLocalEventID = new unsigned int[aRemoteEventID+1];
    // copy old entries
    for (int i=0; i<fRemoteToLocalEventIDLength; i++)
      newRemoteToLocalEventID[i] = fRemoteToLocalEventID[i];
    // init new entries
    for (int i=fRemoteToLocalEventIDLength; i<aRemoteEventID; i++)
      newRemoteToLocalEventID[i] = INVALID_EVENT_ID;
    fRemoteToLocalEventIDLength = aRemoteEventID+1;
    delete(fRemoteToLocalEventID);
    fRemoteToLocalEventID = newRemoteToLocalEventID;
  }
  fRemoteToLocalEventID[aRemoteEventID] = aLocalEventID;
}

unsigned int IMB::TranslateEventID(unsigned int aRemoteEventID)
{
  if (0<=aRemoteEventID && aRemoteEventID<fRemoteToLocalEventIDLength)
    return fRemoteToLocalEventID[aRemoteEventID];
  else
    return INVALID_EVENT_ID;
}

int IMB::ReadEventIDFixed(unsigned int &aEventIDFixed)
{
  return Read((byte *)&aEventIDFixed, 2);
}

int IMB::Read(unsigned int &aValue)
{
  int res=1;
  DebugOut("RU(");
  int i=(*fStream).read();
  if (i<0)
  {
    DebugOut("~");
    while (i<0)
      i=(*fStream).read();
  }
  fCheckSum += i;
  if (i<128)
    aValue = i;
  else
  {
    unsigned int newValue;
    res += Read(newValue);
    aValue = (newValue << 7) || (i & 0x7F); 
  }
  DebugOut(aValue);
  DebugOut(")");
  return res;
}

int IMB::Read(int &aValue)
{
  DebugOut("RI(");
  unsigned int uValue;
  int res=Read(uValue);
  if (uValue & 1)
    aValue = -(uValue >> 1);
  else
    aValue = uValue >> 1;
  DebugOut(aValue);
  DebugOut(")");
  return res;
}

int IMB::Read(float &aValue)
{
  DebugOut("RF(");
  int res=Read((byte*)&aValue, sizeof(float));
  DebugOut(aValue);
  DebugOut(")");
  return res;
}

int IMB::Read(byte* aValue, int aValueSize)
{
  int res=0;
  int err=0;
  DebugOut("RB(");
  DebugOut(aValueSize);
  while (aValueSize>0 && err<3)
  {
    byte br = (*fStream).readBytes((char*)aValue, aValueSize);
    if (br>0)
    {
      for (int i=0; i<br; i++)
        fCheckSum += aValue[i];
      aValueSize -= br;
      aValue += br;
      res += br;
      DebugOut(">");
      DebugOut(br);
    }
    else
    {
      DebugOut("~");
      err++;
    }
  }
  if (aValueSize>0)
  {
    DebugOut("##");
    fErr++;
  }
  DebugOut(")");
  return res;
}

int IMB::Read(String &aValue)
{
  DebugOut("RS(");
  unsigned int len;
  int res = Read(len);
  DebugOut("l=");
  DebugOut(len);
  DebugOut(":");
  // define buffer
  char buffer[len+1];
  // fill buffer
  res += Read((byte*)buffer, len);
  // add 0 at end
  buffer[len] = 0;
  // convert to string
  aValue = String(buffer);
  DebugOut(aValue);
  DebugOut(")");
  return res;
}

int IMB::ReadGUID(byte* aValue)
{
  // fill guid with zeros
  memset(aValue, 0, 16);
  DebugOut("RG(");
  unsigned int len;
  int res = Read(len);
  if (len>16)
  {
    fErr++;
    len = 16;
  }
  res += Read(aValue, len);
  DebugOut(")");
  return res;
}

int IMB::SkipBytes(int aByteCount)
{
  DebugOut("SB(");
  DebugOut(aByteCount);
  if (aByteCount>0)
  {
    DebugOut(":");
    // create temp buffer
    byte buffer[aByteCount];
    // fill temp buffer
    int res = Read(buffer, aByteCount);
    DebugOut(")");
    return res;
  }
  else
  {
    DebugOut(")");
    return 0;
  }
}

int IMB::BufferRead(byte* &aBuffer, unsigned int &aValue)
{
  int res=1;
  byte b=*(aBuffer++);
  if (b<128)
    aValue = b;
  else
  {
    unsigned int newValue;
    res += BufferRead(aBuffer, newValue);
    aValue = (newValue << 7) || (b & 0x7F); 
  }
  return res;
}

int IMB::BufferRead(byte* &aBuffer, int &aValue)
{
  unsigned int uValue;
  int res=BufferRead(aBuffer, uValue);
  if (uValue & 1)
    aValue = -(uValue >> 1);
  else
    aValue = uValue >> 1;
  return res;
}

int IMB::BufferRead(byte* &aBuffer, float &aValue)
{
  return BufferRead(aBuffer, (byte*)&aValue, sizeof(float));
}

int IMB::BufferRead(byte* &aBuffer, byte* aValue, int aValueSize)
{
  memcpy(aValue, aBuffer, aValueSize);
  aBuffer += aValueSize;
  return aValueSize;
}

int IMB::BufferRead(byte* &aBuffer, String& aValue)
{
  unsigned int len;
  int res = BufferRead(aBuffer, len);
  char buffer[len+1];
  res += BufferRead(aBuffer, (byte*)buffer, len);
  // add terminating 0
  buffer[len] = 0;
  // convert to string
  aValue = String(buffer);
  return res;
}

bool IMB::testCheckSum(int aFillerBytes)
{
  // read rest of command if size is to small
  // todo: also for serial?
  if (aFillerBytes>0)
    SkipBytes(aFillerBytes);
  if (fUseCheckSum)
  {
    // read checksum
    int b=(*fStream).read();
    while (b<0)
    {
      DebugOut("~");
      b=(*fStream).read();
    }
    // test checmsum
    if ((b & 0xFF)==(fCheckSum & 0xFF))
      return true;
    else
    {
      HandleCheckSumError();
      DebugOut("##CS(");
      DebugOut(b & 0xFF);
      DebugOut("!=");
      DebugOut(fCheckSum & 0xFF);
      DebugOut(")");
      return false;
    }
  }
  else
    return true; // checksum is not used so return success
}

void IMB::HandleCommand()
{
  int magic=(*fStream).read();
  if (magic>=0)
  {
    DebugOut("RM(");
    DebugOut(magic);
    DebugOut(")");
    if (magic==imbMagic)
    {
      fCheckSum = -1; //  init checksum
      fCheckSum += magic;
      DebugOut("$");
      int sizePayload;
      // number of bytes for header and size
      int sizeHeader=1+Read(sizePayload); 
      if (sizePayload>0)
      {
        DebugOut("E");
        DebugOut(sizePayload);
        DebugOut("(");
        unsigned int remoteEventIDFixed;
        int readPayload = ReadEventIDFixed(remoteEventIDFixed);
        DebugOut("r=");
        DebugOut(remoteEventIDFixed);
        unsigned int localEventID = TranslateEventID(remoteEventIDFixed);
        DebugOut(" -> l=");
        DebugOut(localEventID);
        int eventPayloadSize = sizePayload-readPayload;
        byte eventPayload[eventPayloadSize];
        int res = (*fStream).readBytes((char*)eventPayload, eventPayloadSize);
        if (res==eventPayloadSize)
        {
          for (int i=0; i<res; i++)
            fCheckSum += eventPayload[i];
          DebugOut(")");
          if (testCheckSum(imbMinimumPacketSize-(sizeHeader+sizePayload)))
            HandleEvent(localEventID, eventPayload, res);
          fEventsReceived++;
        }
        else
        {
          fErr++;
          // todo: handle event payload error
          DebugOut("##EP");
        }
      }
      else
      {
        sizePayload = -sizePayload;
        DebugOut("C");
        DebugOut(sizePayload);
        DebugOut("(");
        int command;
        int readPayload = Read(command);
        DebugOut("c=");
        DebugOut(command);
        DebugOut(")");
        // decoding storage
        unsigned int localEventID;
        unsigned int remoteEventID;
        String dummyModelName;
        int dummyModelID;
        unsigned int dummyReconnectable;
        unsigned int dummyState;
        // handle command
        switch (command)
        {
          case icSetEventIDTranslation:
            readPayload += Read(localEventID);
            readPayload += Read(remoteEventID);
            AddOrSetRemoteToLocalEventID(remoteEventID, localEventID);
            HandleCommandSetEventIDTranslation(remoteEventID, localEventID);
            DebugOut(" et(");
            DebugOut(remoteEventID);
            DebugOut("->");
            DebugOut(localEventID);
            DebugOut(") ");
            break;
          case icClose:
            HandleCommandClose();
            SignalClose();
            break;
          case icConnect:
            readPayload += Read(dummyModelName);
            readPayload += Read(dummyModelID);
            readPayload += ReadGUID(uniqueClientID);
            readPayload += ReadGUID(hubID);
            readPayload += Read(dummyReconnectable);
            readPayload += Read(dummyState);
            HandleCommandConnect(uniqueClientID, hubID);
            break;
          case icHeartBeat:
            readPayload += HandleCommandHeartBeat(sizePayload-readPayload);
            break;
          default:
            readPayload += HandleOtherCommand(command, sizePayload-readPayload);
            break;
        }
        // read rest of non-processed payload
        readPayload += SkipBytes(sizePayload-readPayload);
        fCommandsReceived++;
        testCheckSum(imbMinimumPacketSize-(sizeHeader+sizePayload));
      }
      DebugOutLn();
    }
    else
    {
      fMissedMagic++;
      HandleDidNotFindMagic(magic & 0xFF);
    }
  }
}

int IMB::HandleCommandHeartBeat(int aCommandPayloadSize)
{
  if (aCommandPayloadSize>0)
  {
    byte buffer[aCommandPayloadSize];
    int res = Read(buffer, aCommandPayloadSize);
    // asume header message
    SignalConnect(fModelName, fModelID, fReconectable);
    HandleInit(buffer, aCommandPayloadSize);
    fWritingEnabled = true;
    return res;
  }
  else
    return 0;
}

int IMB::Prepare(unsigned int aValue)
{
  if (aValue<128)
    return 1;
  else
    return 1+Prepare((unsigned int)(aValue >> 7));
}

int IMB::Prepare(int aValue)
{
  if (aValue<0)
    return Prepare((unsigned int)(((-aValue) << 1) | 1));
  else
    return Prepare((unsigned int)(aValue << 1));
}

int IMB::Prepare(float aValue)
{
  return sizeof(float);
}

int IMB::Prepare(char* aValue)
{
  unsigned int len = strlen(aValue);
  return Prepare(len)+len;
}

int IMB::BufferWrite(byte* &aBuffer, unsigned int aValue)
{
  if (aValue<128)
  {
    *(aBuffer++)= (byte)aValue;
    return 1;
  }
  else
  {
    *(aBuffer++)= (byte)(aValue & 0x7F) | 0x80;
    return BufferWrite(aBuffer, (unsigned int)(aValue >> 7))+1;
  }
}

int IMB::BufferWrite(byte* &aBuffer, float aValue)
{
  return BufferWrite(aBuffer, (byte*)&aValue, sizeof(float));
}

int IMB::BufferWrite(byte* &aBuffer, int aValue)
{
  if (aValue<0)
    return BufferWrite(aBuffer, (unsigned int)((-aValue) << 1) | 1);
  else
    return BufferWrite(aBuffer, (unsigned int)(aValue << 1));
}

int IMB::BufferWrite(byte* &aBuffer, byte* aValue, int aValueSize)
{
  memcpy(aBuffer, aValue, aValueSize);
  aBuffer += aValueSize;
  return aValueSize; 
}

int IMB::BufferWrite(byte* &aBuffer, char* aValue)
{
  unsigned int len = strlen(aValue);
  int res = BufferWrite(aBuffer, len);
  memcpy(aBuffer, aValue, len);
  aBuffer += len;
  res += len;
  return res; 
}

int IMB::BufferWriteGUIDEmpty(byte* &aBuffer)
{
  unsigned int len = 16;
  int res = BufferWrite(aBuffer, len);
  memset(aBuffer, 0, len);
  aBuffer += len;
  res += len;
  return res;
}

void IMB::FlushCommand(byte* aBuffer, int aSize) 
{ 
  if (fUseCheckSum)
  {
    byte b=checkSum(aBuffer, aSize) & 0xFF;
    (*fStream).write(&b, sizeof(b));
  }
  (*fStream).flush(); 
};

// signal functions

void IMB::SignalConnect(const char* aModelName, int aModelID, bool aReconnectable)
{
  DebugOut("SCO(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icsClient);
  sizePayload += Prepare((unsigned int)aReconnectable); // fReconnectable
  sizePayload += 17; // fHubID
  sizePayload += 17; // uniqueClientID
  sizePayload += Prepare(aModelID);
  sizePayload += Prepare((char*)aModelName);
  sizePayload += Prepare(icConnect);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic;
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icConnect);
  BufferWrite(bufferPtr, (char*)aModelName);
  BufferWrite(bufferPtr, aModelID);
  BufferWriteGUIDEmpty(bufferPtr);
  BufferWriteGUIDEmpty(bufferPtr);
  BufferWrite(bufferPtr, (unsigned int)aReconnectable);
  BufferWrite(bufferPtr, icsClient);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOutLn(")");
  fCommandsSend++;
}

void IMB::SignalSubscribe(unsigned int aEventID, const char* aEventName)
{
  DebugOut("SSU(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icSubscribe);
  sizePayload += Prepare(aEventID);
  sizePayload += Prepare((char*)aEventName);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic; 
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icSubscribe);
  BufferWrite(bufferPtr, aEventID);
  BufferWrite(bufferPtr, (char*)aEventName);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOutLn(")");
  fCommandsSend++;
}

void IMB::SignalPublish(unsigned int aEventID, const char* aEventName)
{
  DebugOut("SPU(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icPublish);
  sizePayload += Prepare(aEventID);
  sizePayload += Prepare((char*)aEventName);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic;
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icPublish);
  BufferWrite(bufferPtr, aEventID);
  BufferWrite(bufferPtr, (char*)aEventName);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOutLn(")");
  fCommandsSend++;
}

void IMB::SignalUnSubscribe(unsigned int aEventID)
{
  DebugOut("SUS(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icUnsubscribe);
  sizePayload += Prepare(aEventID);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic;
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icUnsubscribe);
  BufferWrite(bufferPtr, aEventID);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOutLn(")");
  fCommandsSend++;
}

void IMB::SignalUnPublish(unsigned int aEventID)
{
  DebugOut("SUP(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icUnpublish);
  sizePayload += Prepare(aEventID);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic;
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icUnpublish);
  BufferWrite(bufferPtr, aEventID);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOutLn(")");
  fCommandsSend++;
}

void IMB::SignalNoDelay(bool aNoDelay)
{
  DebugOut("SND(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icSetNoDelay);
  sizePayload += Prepare((unsigned int)aNoDelay);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic;
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icSetNoDelay);
  BufferWrite(bufferPtr, (unsigned int)aNoDelay);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOutLn(")");
  fCommandsSend++;
}

void IMB::SignalClose()
{
  fWritingEnabled = false;
  fConnected = false;
  DebugOut("SCL(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icClose);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic;
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icClose);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOutLn(")");
  fCommandsSend++;
}

void IMB::SignalHeartBeat()
{
  DebugOut("SHB(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += Prepare(icHeartBeat);
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(-sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic;
  BufferWrite(bufferPtr, -sizePayload);
  BufferWrite(bufferPtr, icHeartBeat);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOutLn(")");
  fCommandsSend++;
}

void IMB::SignalEvent(unsigned int aEventID, byte* aData, int aSizeData)
{
  DebugOut("SE(");
  // check how many bytes will be send
  int sizePayload = 0;
  sizePayload += aSizeData+2; // +event id (fixed)
  // add size it self and header byte to command size and test minimum packet size
  int sizeCommand = max(sizePayload+Prepare(sizePayload)+1, imbMinimumPacketSize); 
  // buffer command
  byte buffer[sizeCommand];
  byte* bufferPtr = buffer; 
  *(bufferPtr++) = imbMagic;
  BufferWrite(bufferPtr, sizePayload);
  BufferWrite(bufferPtr, (byte*)&aEventID, 2);
  BufferWrite(bufferPtr, aData, aSizeData);
  (*fStream).write(buffer, sizeCommand);
  FlushCommand(buffer, sizeCommand);
  DebugOut(aEventID);
  DebugOut(":");
  #if defined(DebugToStream)
  for (int i=0; i<sizeCommand; i++)
  {
    if (i>0)
      DebugOut(" ");
    printHex(*fDebugStream, buffer[i]);
  }
  #endif
  DebugOutLn(")");
  fEventsSend++;
}

